const posts = [
    {title:'Post one', bosy: 'This is post one'},
    {title:'Post two', bosy: 'This is post two'}
];

function getPosts (){
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index) => {
            output += post.title + ' ';
        });
        console.log(output); 
    },1000)
}

function createPost (post){
    return new Promise((resolve,reject) =>{
        setTimeout(()=> {
            posts.push(post);
            resolve() || reject('Something Error Went Wrong');
        },2000);
    })

}
createPost({title: 'Post Three', bosy: 'This is post three'})
.then(getPosts)
.catch (err => console.log(err));