const data = {
    "first_name" : "Sammy",
    "last_name" : "Shark",
    "location" : "Ocean",
    "websites" : [
        {
            "description" : "work",
            "URL" : "https://www.digitalocean.com/"
        },
        {
            "description" : "tutorials",
            "URL" : "https://www.digitalocean.com/community/tutorials"
        }
    ],
    "social_media" : [
        {
            "description" : "twitter",
            "URL" : "https://twitter.com/digitalocean"
        },
        {
            "description" : "facebook",
            "URL" : "https://facebook.com/digitalocean"
        },
        {
            "description" : "github",
            "URL" : "https://github.com/digitalocean"
        }
    ]
}
console.log(data.first_name);
console.log(data.last_name);
console.log(data.location);

// for(i = 0; i < data.websites.length; i++){
// console.log(data.websites[i].description);
// console.log(data.websites[i].URL)
// }
// for(i = 0; i < data.social_media.length; i++){
//     console.log(data.social_media[i].description);
//     console.log(data.social_media[i].URL);
// }

data.websites.forEach(index =>{
    console.log(index.description);
    console.log(index.URL);
});
data.social_media.forEach(index =>{
    console.log(index.description);
    console.log(index.URL);
}); 